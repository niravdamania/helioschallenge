
import Foundation
import Combine // <-- Need to import this

/** A simple example demonstrating how to use SensorReader */
class SensorReaderExample {
    private var subscriptions = [AnyCancellable]()

    func start() {
        SensorReader.shared.readerInterval = 2

        subscriptions.append(
            SensorReader.shared.sensorReadingsPublisher
                .receive(on: DispatchQueue.main)
                .sink { sensorReading in
                    // Your code here:
                    // Do something with reading...
                    print("Got reading at \(sensorReading.time)")
                    
                    
                
                }
        )

        SensorReader.shared.startSensorReadings()
    }

    func stop() {
        SensorReader.shared.stopSensorReadings()
    }
}
