import Foundation

struct SensorReading {
    let temp: Double
    let humidity: Double
    let pressure: Int
    let time: Date
}

extension SensorReading: Comparable {
    static func < (lhs: SensorReading, rhs: SensorReading) -> Bool {
        lhs.time < rhs.time
    }
}
