//
//  ViewController.swift
//  HeliosChallenge
//
//  Created by Tejas Thanki on 19/07/21.
//

import UIKit
import Combine

class ViewController: UIViewController {

    //OUTLETS HERE
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    @IBOutlet weak var lblCurrentTimeStamp: UILabel!
    @IBOutlet weak var progressReadingFreqSlider: UISlider!
    @IBOutlet weak var lblFreqReading: UILabel!
   
    @IBOutlet weak var lblMinTemp: UILabel!
    @IBOutlet weak var lblMaxTmp: UILabel!
    @IBOutlet weak var lblAvgTmp: UILabel!
    
    @IBOutlet weak var lblMinHumidity: UILabel!
    @IBOutlet weak var lblMaxHumidity: UILabel!
    @IBOutlet weak var lblAvgHumidity: UILabel!
    
    @IBOutlet weak var lblMinPressure: UILabel!
    @IBOutlet weak var lblMaxPressure: UILabel!
    @IBOutlet weak var lblAvgPressure: UILabel!
    
    @IBOutlet weak var progressReportSizeSlider: UISlider!
    @IBOutlet weak var lblReportSize: UILabel!
    
    @IBOutlet weak var progressReportIntervalSlider: UISlider!
    @IBOutlet weak var lblReportInterval: UILabel!
    
    //VARIABLE
    private var subscriptions = [AnyCancellable]()
    var arrLiveSensorReading = [SensorReading]()
    var counter = 0
    var reportInterval = 2
    var counterArray = 0
    var reportSize = 4
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        startSensorReading(interval: 2)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopReading()
    }
    
    //MARK: - SLIDER FOR FREQUENCY
    @IBAction func progressReadingFreqSlide(_ sender: Any) {
        let sliderValue = progressReadingFreqSlider.value
        let newSlideValue = Int(sliderValue * 10)
        lblFreqReading.text = String(newSlideValue)
        startSensorReading(interval: newSlideValue)
    }
    
    //MARK: - SLIDER FOR REPORT INTERVAL
    @IBAction func progressReportIntervalSlide(_ sender: UISlider) {
        let sliderValue = sender.value
        let newSlideValue = Int(sliderValue * 10)
        lblReportInterval.text = String(newSlideValue)
    }
    //MARK: - SLIDER FOR REPORT SIZE
    @IBAction func progressReportSizeSlide(_ sender: UISlider) {
        let sliderValue = sender.value
        let newSlideValue = Int(sliderValue * 10)
        lblReportSize.text = String(newSlideValue)
        reportSize = newSlideValue
    }
}
extension ViewController {
    
    //MARK: - GET DATA FOR WHEATHER
    ///GET Data for weather and assign it to respected controller, Manage Agregate report for specific interval and size
    /// interval - use for specific time interval for fetch data, arrLiveSensorReading -  store fetched data
    func startSensorReading(interval : Int) {
        SensorReader.shared.readerInterval = TimeInterval(interval)
        
        subscriptions.append(
            SensorReader.shared.sensorReadingsPublisher
                .receive(on: DispatchQueue.main)
                .sink { sensorReading in
                   
                    self.lblTemperature.text =  String(format: "%.2f", sensorReading.temp)
                    self.lblHumidity.text = String(format: "%.2f", sensorReading.humidity)
                    self.lblPressure.text = "\(sensorReading.pressure)"
                    self.lblCurrentTimeStamp.text = "\(sensorReading.time)"
                    self.arrLiveSensorReading.append(sensorReading)
                    self.counterArray += 1
                    
                    if self.counterArray == self.reportInterval{
                        self.counterArray = 0
                        if self.arrLiveSensorReading.count > self.reportSize && self.arrLiveSensorReading.count > (self.counter + self.reportSize){
                            self.getAggregateSensorReading()
                        }
                    }
                }
        )
        SensorReader.shared.startSensorReadings()
    }
    //MARK: - STOP SENSOR READING DATA
    func stopReading() {
        SensorReader.shared.stopSensorReadings()
    }
    //MARK: - GET AGGREGATE SENSOR READING
    ///Manage AGGREGATE SENSOR Reading and assign it to respected control, counter -  for manage spefic index for counter, reportSize - for get sepcifc  size of report from array
    func getAggregateSensorReading(){
        
        let firstIndex = self.counter
        let lastIndex = (self.counter + reportSize) - 1
        
        let arrInterval = arrLiveSensorReading[firstIndex...lastIndex]
        let minTmp = arrInterval.map({$0.temp}).min()
        let maxTmp = arrInterval.map({$0.temp}).max()
        let avgTmp = countAverage(arrTotal: arrInterval.map({$0.temp}))
        
        lblMinTemp.text = "min-temperature: " + "\(String(format: "%.2f", minTmp!))"
        lblMaxTmp.text = "max-temperature:  " + "\(String(format: "%.2f", maxTmp!))"
        lblAvgTmp.text = "avg-temperature:  " + "\(String(format: "%.2f", avgTmp))"
        
        let minHumidity = arrInterval.map({$0.humidity}).min()
        let maxHumidity = arrInterval.map({$0.humidity}).max()
        let avgHumidity = countAverage(arrTotal: arrInterval.map({$0.humidity}))
        
        lblMinHumidity.text = "min-humidity: " + "\(String(format: "%.2f", minHumidity!))"
        lblMaxHumidity.text = "max-humidity: " + "\(String(format: "%.2f", maxHumidity!))"
        lblAvgHumidity.text = "avg-humidity: " + "\(String(format: "%.2f", avgHumidity))"
        
        let minPressure = arrInterval.map({$0.pressure}).min()
        let maxPressure = arrInterval.map({$0.pressure}).max()
        let arrPressure = arrInterval.map({$0.pressure})
        let countTotalPressure = arrPressure.reduce(0, +)
        let avgPressure = countTotalPressure / arrPressure.count
        
        lblMinPressure.text = "min-pressure: \(minPressure ?? 0)"
        lblMaxPressure.text = "max-pressure: \(maxPressure ?? 0)"
        lblAvgPressure.text = "avg-pressure: \(avgPressure)"
        self.counter += reportInterval
    }
    //MARK: - RETURN AVERAGE VALUE FROM ARRAY
    func countAverage(arrTotal : [Double]) -> Double{
        let countTotal = arrTotal.reduce(0, +)
        return countTotal / Double(arrTotal.count)
    }
}
