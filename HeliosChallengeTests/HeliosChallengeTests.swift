//
//  HeliosChallengeTests.swift
//  HeliosChallengeTests
//
//  Created by Tejas Thanki on 19/07/21.
//

import XCTest
@testable import HeliosChallenge

class HeliosChallengeTests: XCTestCase {
    
    let vc = ViewController()
    

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCheckAverage() throws {
        let number  = [1.0,2.0,3.0]
        let val = vc.countAverage(arrTotal: number)
        XCTAssertEqual(val, 2.0)
    }
    
    func testReportSize() throws {
        let firstIndex = vc.counter
        let lastIndex = (vc.counter + vc.reportSize) - 1
        
        if vc.arrLiveSensorReading.count != 0{
            let arrInterval = vc.arrLiveSensorReading[firstIndex...lastIndex]
            XCTAssertEqual(vc.reportInterval, arrInterval.count)
        }
    }
}
